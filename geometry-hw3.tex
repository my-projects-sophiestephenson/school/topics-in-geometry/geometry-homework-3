\documentclass[12pt, twoside]{amsart}

\newcommand{\R}{{\mathbb R}}
\newcommand{\Z}{{\mathbb Z}}
\newcommand{\C}{{\mathbb C}}
\newcommand{\xn}{{\vec x_n}}

\setlength{\textwidth}{6.5in}
\setlength{\textheight}{8.5in}
\setlength{\oddsidemargin}{0in}
\setlength{\evensidemargin}{0in}
\setlength{\topmargin}{0in}

\everymath{\displaystyle}
\usepackage{commath}
\usepackage{textcomp}


\begin{document}


\centerline{\large \sc Topics in Geometry - Homework 3}
\centerline{\large  Sophie Stephenson \ October 7, 2019}
\bigskip

\textbf{Workshop Problems}\\

\begin{enumerate}


\item[4.)] Give another example of a glide reflection (in the form $f(z) = \alpha \bar{z} + \beta$) where the line of reflection is the y-axis.
\begin{proof}[Solution] Since the line of reflection is the y-axis, the angle of inclination is $\theta = \pi/2$. A translation in the direction of the y-axis is a translation by $i$. Since
$$e^{i\pi} = \cos\pi + i\sin\pi = -1,$$
our glide reflection is $f(z) = e^{i\pi}\bar{z} + i = -\bar{z} + i.$\\
\end{proof}
\bigskip

\item[5.)] Prove that if $\alpha, \beta \in \C$ and $|\alpha| = 1$, then $f(z) = \alpha\bar{z} + \beta$ and $g(z) = \alpha z + \beta$ are both isometries of $\C$. (Recall that the complex distance between two points $z, w \in \C$ is $\abs{z - w}$.)
\begin{proof} We will show $f$ and $g$ are isometries of $\C$. For $z, w \in \C$, we have
$$|f(z) - f(w)| = |\alpha \bar{z} + \beta - \alpha \bar{w} - \beta| = |\alpha (\bar{z} - \bar{w})| = |\alpha||\bar{z} - \bar{w}| = |\overline{z - w}| = |z - w|.$$
Then, for $g$,
$$|g(z) - g(w)| = |\alpha z + \beta - \alpha w - \beta| = |\alpha ( z - w)| = |\alpha| |z - w| = |z - w|.$$
Thus, we see that $f$ and $g$ are isometries of $\C$.\\
\end{proof}
\bigskip

\item[7.)] Prove that $T(z) = 1/z$ maps the upper half of the plane (the points $z$ of the form $z = re^{i\theta}$ where $0 < \theta < \pi$) onto the lower half of the plane.
\begin{proof} Consider any point on the upper half of the plane $u = e^{i\theta}$ for $0 < \theta < \pi$. Then,
$$T(u) = \frac{1}{u} = \frac{1}{e^{i\theta}} = e^{-i\theta} = e^{i(-\theta)} = e^{i(2\pi -\theta)}.$$\\
Because $0 < \theta < \pi$, we have that $\pi < 2\pi - \theta < 2\pi$, so $T(u)$ is in the bottom half of the plane, as desired.\\
\end{proof}
\bigskip

\newpage

\item[8.)] Prove that $T$ fixes the real axis as a set. However, it doesn't fix each point. Describe geometrically or algebraically how $T$ acts on the real axis.
\begin{proof} Consider some $r \in \R$. We see that $T(r) = 1/r \in \R$. Thus, $T$ maps every number on the real axis to another number on the real axis, particularly its multiplicative inverse, and it fixes $1$.

\end{proof}
\bigskip


\item[9.)] Let $f: \C \rightarrow \C$ be given by $f(z) = kz$ where $k \in \R, k > 0$. Then $f$ is called a \textit{homothetic transformation} of $\C$. For example, $f(z) = 2z$ is a homothetic transformation that could be described as stretching the plane away from 0 by a factor of 2. Prove that homothetic transformations are not isometries of $\C$. 
\begin{proof} Consider $h(z) = kz$ for some $k \in R, k > 0$ and take $z, w \in \C$. We have that 
$$|h(z) - h(w)| = |kz - kw| = |k(z - w)| = k|z - w|.$$
For $k \not = 1$, we can never have $|z - w| = |h(z) - h(w)|$. Hence, the homothetic transformation $h$ is not an isometry. 

\end{proof}
\bigskip

\item[10.)] Find the equation for the homothetic transformation that shrinks the plane by a factor of 1/2 toward the point $i$. (This can be achieved by composing three different functions - we've done this kind of computation many times before.)
\begin{proof}[Solution] For any $z \in \C$, we must first translate the point by $i$, then shrink by 1/2, then finally translate the point back by $-i$. This can be written as
$$h(z) = (\tfrac{1}{2}(z - i) + i) = \tfrac{1}{2}z - \tfrac{1}{2}i+ i = \tfrac{1}{2}z +\tfrac{1}{2}i,$$
and $h$ is our desired equation.\\
\end{proof}
\bigskip

\end{enumerate}

\bigskip

\textbf{Book Problems}\\

\begin{enumerate}

\item[5c.)] Find a formula for the rotation by -60\textdegree \ about 3.
\begin{proof}[Solution] Since -60\textdegree $= -\pi/3 = 5\pi/3$, our formula is
$$f(z) = e^{i(5\pi/3)}(z - 3) + 3 = e^{i(5\pi/3)}z - 3\big(e^{i(5\pi/3)} + 1\big).$$

\end{proof}
\bigskip

\newpage

\item[7.)] Find a formula for a transformation of the complex plane that carries the unit disk $\{ z : \abs{z} < 1\}$ to the disk $\{z: \abs{z - 5} < 3 \}$. Find a second transformation that does the same thing.
\begin{proof}[Solution] Two transformations which perform this operation are 
$$f(z) = 3z + 5, \ \ \ \ g(z) =  e^{i\pi}(\overline{3z - 5}).$$ The first scales each point in the disk by 3, then translates it by 5; the second scales each point by 3, translates it by -5, then reflects over the y-axis.\\

\end{proof}
\bigskip

\item[15.)] For these complex functions, find $f(\infty)$: \\
\begin{enumerate}
	\item[a.)] $f(z) = \frac{1}{z + 3}$
	\begin{proof}[Solution] $f(\infty) = 0$.\\
	\end{proof}
	
	\item[c.)] $f(z) = \frac{2z - i}{iz + i}$
	\begin{proof}[Solution] $f(\infty) =  \frac{2}{i} = -2i$.
	\end{proof}
\end{enumerate}
\bigskip


\end{enumerate}


\end{document}